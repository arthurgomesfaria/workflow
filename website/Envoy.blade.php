@servers(['web' => 'deployer@arthur-faria-1.paiza-user-free.cloud -p 52800', 'prod' => 'deployer@arthur-faria-1.paiza-user-free.cloud -p 52800'])

@setup
    $repository = 'git@gitlab.com:arthurgomesfaria/workflow.git';
    $releases_dir = '/var/www/html/app/releases';
    $app_dir = '/var/www/html/app';
    $release = date('YmdHis');
    $new_release_dir = $releases_dir .'/'. $release;
@endsetup

@story('deployProd',  ['on' => ['prod'], 'parallel' => true])
    clone_repository
    run_composer
    update_symlinks
@endstory


@story('deploy', ['on' => ['web'], 'parallel' => true])
    clone_repository
    run_composer
    update_symlinks
@endstory

@task('clone_repository')
    echo 'Cloning repository'
    [ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
    git clone --depth 1 {{ $repository }} {{ $new_release_dir }}
    cd {{ $new_release_dir }}
    git reset --hard {{ $commit }}
@endtask

@task('run_composer')
    echo "Starting deployment ({{ $release }})"
    cd {{ $new_release_dir }}/website
    composer install --prefer-dist --no-scripts -q -o
@endtask

@task('update_symlinks')
    echo "Linking storage directory"
    rm -rf {{ $new_release_dir }}/website/storage
    ln -nfs {{ $app_dir }}/website/storage {{ $new_release_dir }}/website/storage

    echo 'Linking .env file'
    ln -nfs {{ $app_dir }}/website/.env {{ $new_release_dir }}/website/.env

    echo 'Linking current release'
    ln -nfs {{ $new_release_dir }}/website {{ $app_dir }}/current
@endtask

